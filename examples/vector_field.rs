use sb_rust_library::{
  plotter::{
    BLUE,
    GREY,
    Orientation,
    Plot,
    RED,
  },
  math::{
    VectorMath,
    Point,
  },
};

const SMALL_CIRCLE_RADIUS: f64 = 0.30;
const SMALL_CIRCLE_SHARPNESS: f64 = 20.0;
const LINE_RADIUS: i64 = 5;

fn get_dydx_vector(p: &Point) -> Point {
  let circles = [(-0.5, -0.5), (0.5, -0.5)];
  let mut dydx_vectors = Vec::new();
  let mut weights = Vec::new();
  dydx_vectors.push((p.y, -p.x));
  weights.push(1.0);

  for c in circles.iter() {
    let d = (p.x - c.0, p.y - c.1);
    let r = d.norm();
    let w = 100.0 * ( -( SMALL_CIRCLE_SHARPNESS * (r - SMALL_CIRCLE_RADIUS)).tanh() + 1.0);
    dydx_vectors.push((-d.1, d.0));
    weights.push(w);
  }
  dydx_vectors = dydx_vectors.iter().zip(weights.iter()).map(|(vec, weight)| vec.mul(*weight)).collect();

  let mut summed_dydx = Point::new(0.0, 0.0);
  for dydx in dydx_vectors.iter() {
    summed_dydx.x += dydx.0;
    summed_dydx.y += dydx.1;
  }
  summed_dydx
}

pub fn main() {
  let mut p = Plot::new(800, 800)
    .with_canvas_color(GREY)
    .with_bg_color(BLUE)
    .with_plotting_range(-1.0..1.0, -1.0..1.0)
    .with_drawing_bounds(0.05..0.95, 0.1..0.95);

  // Make a 20 x 20 grid of x, y values
  let half_n = 15;
  let points: Vec<Point> = (-half_n..half_n)
    .map(|xi| (-half_n..half_n)
      .map(|yi| {
        ((xi as f64) / (half_n as f64), (yi as f64) / (half_n as f64)).into()
      })
      .collect::<Vec<Point>>())
    .flatten()
    .collect();

  // For each (x,y) value, compute the orientation
  let orientations: Vec<f64> = points.iter()
    .map(|p| {
      let dydx = get_dydx_vector(p);
      dydx.y.atan2(dydx.x)
    })
    .collect();

  let orientation = Orientation::new(RED, LINE_RADIUS);
  p.draw_orientations(&orientation, &points, &orientations);
  p.save("vector_field.bmp").unwrap();
}