use std::ops::Range;

/// A 2D vector over the reals.
#[derive(Clone, Copy)]
pub struct Vector {
  /// x component of this vector.
  pub x: f64,
  /// y component of this vector.
  pub y: f64,
}

/// A 2D point in real space.
#[derive(Clone, Copy)]
pub struct Point {
  /// x coordinate of this point.
  pub x: f64,
  /// y coordinate of this point.
  pub y: f64,
}

/// A rectangular region in a 2D vector space over a generic field.
pub struct Bounds<T> {
  /// Boundary of this region in the x-dimension.
  pub x: Range<T>,
  /// Boundary of this region in the y-dimension.
  pub y: Range<T>
}

impl Vector {

  /// Creates a new point struct.
  pub fn new(x: f64, y: f64) -> Vector {
    Vector { x, y }
  }

  /// Computes the norm of this vector.
  pub fn norm(&self) -> f64 {
    (self.x * self.x + self.y * self.y).sqrt()
  }

  /// Computes the 90 degree counterclockwise rotation of this vector.
  pub fn rotate_ccw(&self) -> Vector {
    (- self.y, self.x).into()
  }

  /// Computes the 90 degree clockwise rotation of this vector.
  pub fn rotate_cw(&self) -> Vector {
    ( self.y, -self.x).into()
  }
}

impl Point {

  /// Creates a new point struct.
  pub fn new(x: f64, y: f64) -> Point {
    Point { x, y }
  }

}

impl<T> From<(Range<T>, Range<T>)> for Bounds<T> {
  fn from((x, y): (Range<T>, Range<T>)) -> Self {
    Bounds { x, y }
  }
}


// Vector Addition

impl std::ops::Add<Vector> for Vector {
  type Output = Vector;

  fn add(self, other: Self) -> Vector {
    (self.x + other.x, self.y + other.y).into()
  }
}

impl std::ops::Add<&Vector> for Vector {
  type Output = Vector;

  fn add(self, other: &Self) -> Vector {
    (self.x + other.x, self.y + other.y).into()
  }
}

impl std::ops::Add<Vector> for &Vector {
  type Output = Vector;

  fn add(self, other: Vector) -> Vector {
    (self.x + other.x, self.y + other.y).into()
  }
}

impl std::ops::Add<&Vector> for &Vector {
  type Output = Vector;

  fn add(self, other: &Vector) -> Vector {
    (self.x + other.x, self.y + other.y).into()
  }
}

impl std::ops::AddAssign<Vector> for Vector {
  fn add_assign(&mut self, other: Self) {
    self.x += other.x;
    self.y += other.y;
  }
}

impl std::ops::AddAssign<&Vector> for Vector {
  fn add_assign(&mut self, other: &Self) {
    self.x += other.x;
    self.y += other.y;
  }
}

// Vector Subtraction

impl std::ops::Sub<Vector> for Vector {
  type Output = Vector;

  fn sub(self, other: Self) -> Vector {
    (self.x - other.x, self.y - other.y).into()
  }
}

impl std::ops::Sub<&Vector> for Vector {
  type Output = Vector;

  fn sub(self, other: &Self) -> Vector {
    (self.x - other.x, self.y - other.y).into()
  }
}

impl std::ops::Sub<Vector> for &Vector {
  type Output = Vector;

  fn sub(self, other: Vector) -> Vector {
    (self.x - other.x, self.y - other.y).into()
  }
}

impl std::ops::Sub<&Vector> for &Vector {
  type Output = Vector;

  fn sub(self, other: &Vector) -> Vector {
    (self.x - other.x, self.y - other.y).into()
  }
}

impl std::ops::SubAssign<Vector> for Vector {
  fn sub_assign(&mut self, other: Self) {
    self.x -= other.x;
    self.y -= other.y;
  }
}

impl std::ops::SubAssign<&Vector> for Vector {
  fn sub_assign(&mut self, other: &Self) {
    self.x -= other.x;
    self.y -= other.y;
  }
}

// Vector - Vector multiplication

impl std::ops::Mul<Vector> for Vector {
  type Output = f64;

  fn mul(self, other: Self) -> f64 {
    self.x * other.x + self.y * other.y
  }
}

impl std::ops::Mul<Vector> for &Vector {
  type Output = f64;

  fn mul(self, other: Vector) -> f64 {
    self.x * other.x + self.y * other.y
  }
}

impl std::ops::Mul<&Vector> for Vector {
  type Output = f64;

  fn mul(self, other: &Self) -> f64 {
    self.x * other.x + self.y * other.y
  }
}

impl std::ops::Mul for &Vector {
  type Output = f64;

  fn mul(self, other: &Vector) -> f64 {
    self.x * other.x + self.y * other.y
  }
}

// Vector - Scalar Multiplication

impl std::ops::Mul<f64> for Vector {
  type Output = Vector;

  fn mul(self, scalar: f64) -> Vector {
    (self.x * scalar, self.y * scalar).into()
  }
}

impl std::ops::Mul<f64> for &Vector {
  type Output = Vector;

  fn mul(self, scalar: f64) -> Vector {
    (self.x * scalar, self.y * scalar).into()
  }
}

impl std::ops::Mul<Vector> for f64 {
  type Output = Vector;

  fn mul(self, p: Vector) -> Vector {
    (self * p.x, self * p.y).into()
  }
}

impl std::ops::Mul<&Vector> for f64 {
  type Output = Vector;

  fn mul(self, p: &Vector) -> Vector {
    (self * p.x, self * p.y).into()
  }
}

impl std::ops::MulAssign<f64> for Vector {
  fn mul_assign(&mut self, scalar: f64) {
    self.x *= scalar;
    self.y *= scalar;
  }
}

// Vector - Scalar Division

impl std::ops::Div<f64> for Vector {
  type Output = Vector;

  fn div(self, scalar: f64) -> Vector {
    (self.x / scalar, self.y / scalar).into()
  }
}

impl std::ops::Div<f64> for &Vector {
  type Output = Vector;

  fn div(self, scalar: f64) -> Vector {
    (self.x / scalar, self.y / scalar).into()
  }
}

impl std::ops::DivAssign<f64> for Vector {
  fn div_assign(&mut self, scalar: f64) {
    self.x /= scalar;
    self.y /= scalar;
  }
}

// Negation

impl std::ops::Neg for Vector {
  type Output = Vector;

  fn neg(self) -> Self {
    (-self.x, -self.y).into()
  }
}

impl std::ops::Neg for &Vector {
  type Output = Vector;

  fn neg(self) -> Vector {
    (-self.x, -self.y).into()
  }
}

// Point - Point Subtraction

impl std::ops::Sub<Point> for Point {
  type Output = Vector;

  fn sub(self, other: Self) -> Vector {
    (self.x - other.x, self.y - other.y).into()
  }
}

impl std::ops::Sub<&Point> for Point {
  type Output = Vector;

  fn sub(self, other: &Self) -> Vector {
    (self.x - other.x, self.y - other.y).into()
  }
}

impl std::ops::Sub<Point> for &Point {
  type Output = Vector;

  fn sub(self, other: Point) -> Vector {
    (self.x - other.x, self.y - other.y).into()
  }
}

impl std::ops::Sub<&Point> for &Point {
  type Output = Vector;

  fn sub(self, other: &Point) -> Vector {
    (self.x - other.x, self.y - other.y).into()
  }
}

// Point-Vector Addition

impl std::ops::Add<Vector> for Point {
  type Output = Point;

  fn add(self, other: Vector) -> Point {
    (self.x + other.x, self.y + other.y).into()
  }
}

impl std::ops::Add<&Vector> for Point {
  type Output = Point;

  fn add(self, other: &Vector) -> Point {
    (self.x + other.x, self.y + other.y).into()
  }
}

impl std::ops::Add<Vector> for &Point {
  type Output = Point;

  fn add(self, other: Vector) -> Point {
    (self.x + other.x, self.y + other.y).into()
  }
}

impl std::ops::Add<&Vector> for &Point {
  type Output = Point;

  fn add(self, other: &Vector) -> Point {
    (self.x + other.x, self.y + other.y).into()
  }
}

impl std::ops::AddAssign<Vector> for Point {
  fn add_assign(&mut self, other: Vector) {
    self.x += other.x;
    self.y += other.y;
  }
}

impl std::ops::AddAssign<&Vector> for Point {
  fn add_assign(&mut self, other: &Vector) {
    self.x += other.x;
    self.y += other.y;
  }
}


// Conversion operations

impl From<(f64, f64)> for Vector {
  fn from((x, y): (f64, f64)) -> Self {
    Vector { x, y }
  }
}

impl From<Vector> for (f64, f64) {
  fn from(p: Vector) -> Self {
    (p.x, p.y)
  }
}

impl From<&Vector> for (f64, f64) {
  fn from(p: &Vector) -> Self {
    (p.x, p.y)
  }
}

impl From<(f64, f64)> for Point {
  fn from((x, y): (f64, f64)) -> Self {
    Point { x, y }
  }
}

impl From<Point> for (f64, f64) {
  fn from(p: Point) -> Self {
    (p.x, p.y)
  }
}

impl From<&Point> for (f64, f64) {
  fn from(p: &Point) -> Self {
    (p.x, p.y)
  }
}

impl From<Point> for Vector {
  fn from(p: Point) -> Self {
    Vector { x: p.x, y: p.y }
  }
}

impl From<&Point> for Vector {
  fn from(p: &Point) -> Self {
    Vector { x: p.x, y: p.y }
  }
}

impl From<Vector> for Point {
  fn from(v: Vector) -> Self {
    Point { x: v.x, y: v.y }
  }
}

impl From<&Vector> for Point {
  fn from(v: &Vector) -> Self {
    Point { x: v.x, y: v.y }
  }
}

/// Treats object mathematically as if it were a vector.
///
/// Basically this is a way to perform math on objects like `Vec<f64>` without
/// wrapping them in a struct.
///
/// # Examples
///
/// Basic addition:
/// ```
/// let a = vec![1.0f64, 1.0, 1.0];
/// let b = vec![1.0, 2.0, 3.0];
/// let c = a.add(&b);
/// assert_eq!(c, vec![2.0, 3.0, 4.0]);
/// ```
///
/// Dot products:
/// ```
/// let a = vec![1.0, 1.0, 1.0];
/// let b = vec![1.0, 0.0, 2.0];
/// assert_eq!(a.dot(b), 4.0);
/// ```
pub trait VectorMath: Sized {
  /// Computes dot product of two vectors.
  fn dot(&self, other: &Self) -> f64;

  /// Adds two vectors, returning a new vector.
  fn add(&self, other: &Self) -> Self;

  /// Subtracts two vectors, returning a new vector.
  fn sub(&self, other: &Self) -> Self;

  /// Multiplies a scalar and a vector, returning a new vector.
  fn mul(&self, scalar: f64) -> Self;

  /// Multiplies a vector and a vector pointwise, returning a new vector.
  fn mul_pt(&self, other: &Self) -> Self;

  /// Computes the vector norm
  fn norm(&self) -> f64 {
    self.dot(self).sqrt()
  }

  /// Normalizes the vector to unit length, returning a new vector
  fn normalized(&self) -> Self {
    self.mul(1.0 / self.norm())
  }
}

impl VectorMath for Vec<f64> {
  fn dot(&self, other: &Self) -> f64 {
    self.iter().zip(other.iter()).map(|(x, y)| x * y).sum::<f64>()
  }
  fn add(&self, other: &Self) -> Self {
    self.iter().zip(other.iter()).map(|(x, y)| x + y).collect()
  }
  fn sub(&self, other: &Self) -> Self {
    self.iter().zip(other.iter()).map(|(x, y)| x - y).collect()
  }
  fn mul(&self, scalar: f64) -> Self {
    self.iter().map(|x| scalar * x).collect()
  }
  fn mul_pt(&self, other: &Self) -> Self {
    self.iter().zip(other.iter()).map(|(x, y)| x * y).collect()
  }
}

impl VectorMath for (f64, f64) {
  fn dot(&self, other: &Self) -> f64 {
    self.0 * other.0 + self.1 * other.1
  }
  fn add(&self, other: &Self) -> Self {
    (self.0 + other.0, self.1 + other.1)
  }
  fn sub(&self, other: &Self) -> Self {
    (self.0 - other.0, self.1 - other.1)
  }
  fn mul(&self, scalar: f64) -> Self {
    (scalar * self.0, scalar * self.1)
  }
  fn mul_pt(&self, other: &Self) -> Self {
    (self.0 * other.0, self.1 * other.1)
  }
}
